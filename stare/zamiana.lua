local ugmatch = unicode.utf8.gmatch
local ulower  = unicode.utf8.lower
local upper   = unicode.utf8.upper

local translator = function()
  local obj = {}
  obj.__index = obj
  obj.positions= {[-3]=1, [-2] = 2, [-1] = 3, [1]=4, [2] = 5, [3] = 6}
  obj.init = function(self, seed)
    local litery  = "m j a t s u k w n z e c i h y b o r d p ł ę ó f g ż ą l ś v ź x ń ć q"
    local  seed = seed or litery
    local chars = {}
    for char in ugmatch(seed, "[^%s]" ) do
      table.insert(chars, char)
      -- save also char number under char 
      chars[char] = #chars
    end
    -- make chars available in other methods
    self.chars = chars
    -- initialize grupas
    local grupa = {[0] = {}, [1] = {}, [2] = {}, [3] = {}, [4] = {}}
    local i = 1
    local grupachars = function(grupano, start, stop)
      local j = 0
      for i = start, stop do 
        local char = chars[i+1]
        -- save both 
        grupa[grupano][j] = char
        grupa[grupano][char] = j
        -- print(grupano, j, char)
        j = j + 1
      end
    end
    -- save litery in grupas
    for i=0,4 do
      local start = i * 7
      local stop  = (i+1)*7-1
      grupachars(i, start, stop)
    end
    self.grupa = grupa
    -- return grupa

  end

  obj.encrypt = function(self, text)
    local a = ulower(text)
    local wynik = ""
    local ile = 0
    local poprzedni = ""
    local popgrupa = 10
    local haszg = self.chars
    local grupa  = self.grupa
    local positions = self.positions
    local g
    for char in ugmatch(text, ".") do
      local i = ulower(char)
      -- detect uppercase letter
      local isUpper = i ~= char
      local w = i
      if not haszg[i] then
        poprzedni = ""
        popgrupa = 10
      else
        -- calculate group the char belongs to
        g = math.floor((haszg[i]-1) / 7 )
        -- print(i, haszg[i], g,grupa[g][i])
        -- if char doesn't belong to any group
        if not grupa[g] then  print "niemozliwe"; os.exit(1) end
        k = grupa[g][i] - 3
        if g == popgrupa then
          -- if k < 0 then
          --   w = k + 4
          -- elseif k == 0 then
          --   w = i
          -- else
          --   w = k + 3
          -- end
          w = positions[k] or i
          ile = ile + 1
        else
          w = i
        end
        popgrupa = g
      end
      -- print(i,g,  k, w)
      if isUpper then
        w = upper(w)
      end
      wynik = wynik .. w
    end
    return {ile, wynik}
  end

  local self = setmetatable({}, obj)
  return self

end


-- local transobj = translator()

-- transobj:init()

-- local text = arg[1] or "Nieboq"

-- local result = transobj:encrypt(text)
-- print(result[1], result[2])

return translator
