local texrg = {}

local zamiana = require "zamiana"

local transobj = zamiana()
transobj:init()


local uchar   = unicode.utf8.char
local ubyte   = unicode.utf8.byte
local ugmatch = unicode.utf8.gmatch

local glyph_id = node.id "glyph"
local hlist_id = node.id "hlist"
local vlist_id = node.id "vlist"
local glue_id  = node.id "glue"

-- the font_id of the processed font must be saved from the TeX side
local font_id = nil
local changearray = {}


-- we should call this function whenewer the word in the processed
-- font stops -- at spaces (glue node), at font changes, or when 
-- child hlist is processed
-- maybe there are some more cases, but these are here for now
local function change(nodes)
  -- process only when we have any nodes
  if #nodes > 0 then
    local current = {}
    -- for n in node.traverse(nodes) do
    for _,n in ipairs(nodes) do
      current[#current+1] = uchar(n.char)
    end
    local text = table.concat(current)
    local new = transobj:encrypt(text)[2]
    print("encrypt", text, new)
    if new then
      local chars = {}
      for char in ugmatch(new,".") do
        chars[#chars+1] = ubyte(char)
      end
      for i, char in ipairs(chars) do
        nodes[i].char = char
      end
    end
  end
  -- always reset the node array
  return {}
end

local function process(head)
  local current_nodes = {}
  for n in node.traverse(head) do
    -- we need to recursivelly process \hboxes and \vboxes
    if n.id == hlist_id or n.id == vlist_id then
      current_nodes = change(current_nodes)
      n.head = process(n.head)
    -- end current word at spaces  
    elseif n.id == glue_id then
      current_nodes = change(current_nodes)
    -- process glyphs
    elseif n.id == glyph_id then 
      -- process only the right font
      if n.font == font_id then
        current_nodes[#current_nodes+1] = n
      else
        -- close current word and don't add more characters
        current_nodes = change(current_nodes)
      end
    end
  end
  -- there may be unprocessed word at this moment, we need to close it
  change(current_nodes)
  return head
end

-- process the node list
function texrg.filter(head)
  return process(head)
end


-- the node processing can't work without saved font id
function texrg.register_font(id)
  print("font id", id)
  font_id = id
end

function texrg.exchangechars(old, new)
  -- save the character changes
  changearray[old] = new
end


return texrg
